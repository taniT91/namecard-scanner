# 名刺スキャナー
複数の名刺が写った画像から文字を認識するプログラム  
SanSanが名刺スキャンをやっているが，中では人力でやっていると聞いたので，全自動でできないか挑戦した．

イメージ図
![イメージ図](./readme_imgs/complete_img.png)

## はじめに
完全なものはできませんでした．申し訳ありません．  
認めて頂けるかはわかりませんが，提出します．

## どのように目標を達成しようとしたか
まず，一枚の画像から一枚ずつ名刺を切り出す．  
次に以下の処理をすることで，文字認識をしようとした．

![プロセス](./readme_imgs/process.png)

## できなかったこと
bounding boxから一文字を切り出すプログラムの作成．  
そこでDeep Learningへの入力となる一文字の画像は，手動で切り出している．

## Requirements
Python
- 2.7.12
- anaconda-2.4.0

OpenCV
- 3.0.0

## 実行
複数の名刺が写った画像から，一枚の名刺を切り出す．
```
python main.py
```

一枚の名刺から，文字領域をbounding boxとして切り出す．
```
python region_division.py
```

Deep Learingの部分は，長くなってしまったため，`./chainer/README.md`に分けた．
