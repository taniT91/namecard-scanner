# -*- coding: utf-8 -*
import math
import numpy as np

def included_orth(xmin1, xmax1, xmin2, xmax2):
    return xmin1 <= xmin2 <= xmax1 and xmin1 <= xmax2 <= xmax1 or xmin2 <= xmin1 <= xmax2 and xmin2 <= xmax1 <= xmax2

def distance_orth(xmin1, xmax1, xmin2, xmax2):
    if xmin1 <= xmin2 <= xmax1 or xmin1 <= xmax2 <= xmax1 or xmin2 <= xmin1 <= xmax2:
        return 0
    return min(abs(xmin1 - xmax2), abs(xmax1 - xmin1))

def closs_point(l, m):
    det = np.cross(l[1]-l[0], m[1]-m[0])
    # if det is zero, not crossed or same lines
    return l[0] + (l[1]-l[0]) * np.cross(m[0]-l[0], m[1]-m[0]) / det

def corners(lines):
    points = []
    prev = lines[-1]
    for line in lines:
        points.append(closs_point(prev, line))
        prev = line
    return points

def regression_line(points):
    A = np.array([(p[0], 1) for p in points])
    y = np.array([p[1] for p in points])
    m, c = np.linalg.lstsq(A, y)[0]
    return (np.array([p[0], int(round(m*p[0]+c))]), np.array([p[-1], int(round(m*p[-1]+c))]))

def to_pts(points):
    return [np.array(map(lambda p: np.array([p]), points))]

def color_array(n):
    h = 360/n
    return [hsv_bgr(h*i, 255, 255) for i in xrange(n)]

# s and v are 0 to 255
def hsv_bgr(h, s, v):
    maxv = v
    minv = int(maxv * (1.0 - s / 255.0))
    hsection = h / 60
    if hsection == 0:
        return (minv, int((h/60.0)*(maxv-minv)+minv), maxv)
    elif hsection == 1:
        return (minv, maxv, int(((120-h)/60.0)*(maxv-minv)))
    elif hsection == 2:
        return (int(((h-120)/60.0)*(maxv-minv)+minv), maxv, minv)
    elif hsection == 3:
        return (maxv, int(((240-h)/60.0)*(maxv-minv)+minv), minv)
    elif hsection == 4:
        return (maxv, minv, int(((h-240)/60.0)*(maxv-minv)+minv))
    elif hsection == 5:
        return (int(((360-h)/60.0)*(maxv-minv)+minv), minv, maxv)
    else:
        raise Exception("invalid HSV")
