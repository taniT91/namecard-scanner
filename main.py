# -*- coding: utf-8 -*
import cv2
import numpy as np
import os
import os.path
import connected_component

import params

img_dir = "./images"
output_dir = "./processed"

def write(dir, file, img):
    target_dir = os.path.join(output_dir, dir)
    if not os.path.isdir(target_dir):
        os.makedirs(target_dir)
    cv2.imwrite(os.path.join(target_dir, file), img)


def transform(img, points):
    W = params.CARD_WIDTH * 10
    H = params.CARD_HEIGHT * 10
    xs = [p[0] for p in points]
    ys = [p[1] for p in points]
    width = max(xs) - min(xs)
    height = max(ys) - min(ys)

    if width < height:
        W, H = H, W
    dst = np.array([
        np.array([W-1, H-1]),
        np.array([0, H-1]),
        np.array([0, 0]),
        np.array([W-1, 0]),
        ], np.float32)

    trans = cv2.getPerspectiveTransform(points, dst)
    return cv2.warpPerspective(img, trans, (W, H))


def edge(f, img, original, ratio):
    blur = cv2.GaussianBlur(img, (25,25), 1)
    edge = cv2.Canny(blur, 100, 200)
    kernel = np.ones((2, 2), np.uint8)
    dilation = cv2.dilate(edge, kernel, iterations = 1)
    cc = connected_component.detect(dilation, img, 600)

    for i, c in enumerate(cc.components):
        pts = c.detect_4points()

        out = transform(original, np.array([p[0] for p in pts], np.float32) * ratio)
        write('out', str(i)+f, out)


def resized_img(img, target_max = 1000):
    height, width = img.shape[:2]
    max_side = max(height, width)
    ratio = 1
    while max_side / ratio > target_max:
        ratio *= 2
    return cv2.resize(img, (width/ratio, height/ratio)), ratio

def main():
    files = []
    for file in os.listdir(img_dir):
        if os.path.isfile(os.path.join(img_dir, file)) and file != ".DS_Store":
            files.append(file)

    for f in files:
        img = cv2.imread(os.path.join(img_dir, f))
        print f, img.shape
        resized, ratio = resized_img(img)
        write('resized', f, resized)
        edge(f, resized, img, ratio)

if __name__ == '__main__':
    main()
