# -*- coding: utf-8 -*

class Node:
    def __init__(self, content, parent_list):
        self.content = content
        self.parent_list = parent_list
        self.next = self
        self.prev = self

    def __str__(self):
        return str(self.content)

    def absorb(self, num, fn = lambda x, y: x):
        if num == 0:
            return self
        target = self.next if num > 0 else self.prev
        for i in xrange(abs(num)):
            self.content = fn(self.content, target.content)
            if target is self.parent_list.node:
                self.parent_list.node = self
            if target is self:
                raise Exception("absorb myself!!!")
            target = target.next if num > 0 else target.prev
        if num > 0:
            self.next = target
            target.prev = self
        else:
            self.prev = target
            target.next = self
        return self

class List:
    def __init__(self, list):
        self.node = self._construct(list)

    def __str__(self):
        list = self.to_python_list()
        return "circular_list(" + str(list) + ")"

    def __getitem__(self, index):
        if self.node is None:
            return None
        return self.get_node(index).content

    def get_node(self, index):
        if self.node is None:
            return None
        target = self.node
        for i in xrange(abs(index)):
            target = target.next if index > 0 else target.prev
        return target

    def to_python_list(self):
        list = []
        if self.node:
            node = self.node
            while True:
                list.append(node.content)
                node = node.next
                if node is self.node:
                    break
        return list

    def _construct(self, list):
        if len(list) == 0:
            return None
        nodes = [Node(l, self) for l in list]
        size = len(nodes)
        for i, node in enumerate(nodes):
            node.next = nodes[(i+1)%size]
            node.prev = nodes[i-1]
        return nodes[0]


if __name__ == '__main__':
    l = List(['a', 'b', 'c', 'd', 'e', 'f'])
    print l
    print str(l[1])
    print l.get_node(1).absorb(2, lambda x, y: x + y)
    print l
    print l.get_node(-1).absorb(1, lambda x, y: '*' + x + y + '*')
    print l
    try:
        l.get_node(1).absorb(3, lambda x, y: x + y) # raise error
    except Exception as e:
        print e
