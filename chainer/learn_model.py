# -*- coding: utf-8 -*

import os
os.environ["CHAINER_TYPE_CHECK"] = "0"

import numpy as np
import chainer
from chainer import cuda, Variable, FunctionSet, optimizers, serializers
import chainer.functions  as F
import sys
import cv2
import argparse
from itertools import chain
import codecs

np.set_printoptions(threshold=np.inf)
train_dir = './characters_img/'

alpha_small_lst = [chr(i) for i in range(97, 97+26)]
alpha_big_lst = [chr(i) for i in range(65, 65+26)]
num_lst = [chr(i) for i in range(48, 48+10)]
hiragana_lst = [unichr(i) for i in range(12353, 12436)]
katakana_lst = [unichr(i) for i in range(12449, 12532+1)]
kanji_lst = []
for kanji in codecs.open('jyoyo_ichiran.txt', 'r', 'utf-8'):
    kanji_lst.append(kanji.strip())
char_lst = list(chain.from_iterable([alpha_small_lst, alpha_big_lst, num_lst, hiragana_lst, katakana_lst, kanji_lst]))


def load_train_data(file_dir):
    x_train = []
    y_train = []
    target_dir = train_dir + file_dir

    for file in os.listdir(target_dir):
        if file == '.DS_Store': continue

        src = cv2.imread(os.path.join(target_dir, file), 0)
        ret, x = cv2.threshold(src, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)

        x_train.append(x)
        index = char_lst.index(unicode(file, 'utf-8')[0])
        y_train.append(index)

    return (x_train, y_train)


# Prepare multi-layer perceptron model
# 多層パーセプトロンモデルの設定
# 入力 784次元、出力 y_num次元
def init_model(n_units, y_num):
    return FunctionSet(l1=F.Linear(784, n_units),
                       l2=F.Linear(n_units, n_units),
                       l3=F.Linear(n_units, y_num))


# Neural net architecture
# ニューラルネットの構造
def forward(model, x_data, y_data, train=True):
    x, t = Variable(x_data), Variable(y_data)
    h1 = F.dropout(F.relu(model.l1(x)),  train=train)
    h2 = F.dropout(F.relu(model.l2(h1)), train=train)
    y  = model.l3(h2)
    return F.softmax_cross_entropy(y, t), F.accuracy(y, t)


# Convolutional Neural Network(CNN)
def init_cnn_model(y_num):
    return FunctionSet(conv1=F.Convolution2D(1, 20, 5),
                       conv2=F.Convolution2D(20, 50, 5),
                       l1=F.Linear(800, 500),
                       l2=F.Linear(500, y_num))


# CNNのforward
def forward_cnn(model, x_data, y_data, train=True):
    x, t = chainer.Variable(x_data), chainer.Variable(y_data)
    h = F.max_pooling_2d(F.relu(model.conv1(x)), 2)
    h = F.max_pooling_2d(F.relu(model.conv2(h)), 2)
    h = F.dropout(F.relu(model.l1(h)), train=train)
    y = model.l2(h)
    return F.softmax_cross_entropy(y, t), F.accuracy(y, t)


def main():
    parser = argparse.ArgumentParser(description='Chainer')
    parser.add_argument('--batchsize', '-b', type=int, default=3641,
                        help='Number of images in each mini batch')
    parser.add_argument('--epoch', '-e', type=int, default=20,
                        help='Number of sweeps over the dataset to train')
    parser.add_argument('--gpu', '-g', type=int, default=-1,
                        help='GPU ID (negative value indicates CPU)')
    # parser.add_argument('--num', '-n', type=int, default=17212,
    #                     help='Number of train data')
    args = parser.parse_args()

    print('GPU: {}'.format(args.gpu))
    print('# Minibatch-size: {}'.format(args.batchsize))
    print('# epoch: {}'.format(args.epoch))
    print('')

    gpu_id = args.gpu
    if gpu_id >= 0:
        cuda.check_cuda_available()
    xp = cuda.cupy if gpu_id >= 0 else np

    batchsize = args.batchsize
    n_epoch   = args.epoch
    # 中間層の数
    n_units   = 5000

    print('Loading train data...')
    print('alpha_small')
    x_train_small, y_train_small = load_train_data('alpha_small')
    print('alpha_big')
    x_train_big, y_train_big = load_train_data('alpha_big')
    print('hiragana')
    x_train_hiragana, y_train_hiragana = load_train_data('hiragana')
    print('katakana')
    x_train_katakana, y_train_katakana = load_train_data('katakana')
    print('num')
    x_train_num, y_train_num = load_train_data('num')
    print('kanji')
    x_train_kanji, y_train_kanji = load_train_data('kanji')
    print('Loaded!')

    x_train_small.extend(x_train_big)
    y_train_small.extend(y_train_big)
    x_train_small.extend(x_train_hiragana)
    y_train_small.extend(y_train_hiragana)
    x_train_small.extend(x_train_katakana)
    y_train_small.extend(y_train_katakana)
    x_train_small.extend(x_train_num)
    y_train_small.extend(y_train_num)
    x_train_small.extend(x_train_kanji)
    y_train_small.extend(y_train_kanji)
    N = len(y_train_small)
    print('# data-num: {}'.format(N))
    x_train = np.array(x_train_small).astype(xp.float32).reshape((len(x_train_small), 1, 28, 28)) / 255
    y_train = np.array(y_train_small).astype(xp.int32)

    y_num = len(char_lst)
    model = init_cnn_model(y_num)

    if gpu_id >= 0:
        cuda.get_device(gpu_id).use()
        model.to_gpu()

    # Setup optimizer
    optimizer = optimizers.Adam()
    optimizer.setup(model)

    train_loss = []
    train_acc  = []

    # Learning loop
    for epoch in xrange(1, n_epoch+1):
        print 'epoch', epoch

        # training
        # N個の順番をランダムに並び替える
        perm = np.random.permutation(N)
        sum_accuracy = 0
        sum_loss = 0
        # 0〜Nまでのデータをバッチサイズごとに使って学習
        for i in xrange(0, N, batchsize):
            x_batch = xp.asarray(x_train[perm[i:i+batchsize]])
            y_batch = xp.asarray(y_train[perm[i:i+batchsize]])

            # 勾配を初期化
            optimizer.zero_grads()
            # 順伝播させて誤差と精度を算出
            loss, acc = forward_cnn(model, x_batch, y_batch)

            # 誤差逆伝播で勾配を計算
            loss.backward()
            optimizer.update()
            sum_loss     += float(cuda.to_cpu(loss.data)) * batchsize
            sum_accuracy += float(cuda.to_cpu(acc.data)) * batchsize

        # 訓練データの誤差と、正解精度を表示
        print 'train mean loss={}, accuracy={}'.format(sum_loss / N, sum_accuracy / N)

        train_loss.append(sum_loss / N)
        train_acc.append(sum_accuracy / N)

    # 学習したパラメーターを保存
    model.to_cpu()
    serializers.save_npz('./models/my.model', model)

if __name__ == '__main__':
    main()
