# -*- coding: utf-8 -*

import cv2
import os

test_dir = './imgs'
data_dir = './imgs/test_data'

def trimming_test(img):
    trimmed_imgs = []
    char_lst = []
    # trimmed_imgs.append(img[:, 0:18])  # H
    # char_lst.append('H')
    # trimmed_imgs.append(img[:, 18:26]) # I
    # char_lst.append('I')
    # trimmed_imgs.append(img[:, 26:44]) # D
    # char_lst.append('D')
    # trimmed_imgs.append(img[:, 44:60]) # E
    # char_lst.append('E')
    # trimmed_imgs.append(img[:, 60:76]) # K
    # char_lst.append('K')
    # trimmed_imgs.append(img[:, 76:84]) # I
    # char_lst.append('I')
    #
    # trimmed_imgs.append(img[:, 102:124]) # M
    # char_lst.append('M')
    # trimmed_imgs.append(img[:, 124:142]) # A
    # char_lst.append('A')
    # trimmed_imgs.append(img[:, 142:160]) # T
    # char_lst.append('T')
    # trimmed_imgs.append(img[:, 160:176]) # S
    # char_lst.append('S')
    # trimmed_imgs.append(img[:, 176:192]) # U
    # char_lst.append('U')
    # trimmed_imgs.append(img[:, 192:214]) # M
    # char_lst.append('M')
    # trimmed_imgs.append(img[:, 214:234]) # O
    # char_lst.append('O')
    # trimmed_imgs.append(img[:, 234:252]) # T
    # char_lst.append('T')
    # trimmed_imgs.append(img[:, 252:270]) # O
    # char_lst.append('O')

    trimmed_imgs.append(img[:, 0:50]) # 松
    char_lst.append('松')

    trimmed_imgs.append(img[:, 75:125]) # 元
    char_lst.append('元')

    trimmed_imgs.append(img[:, 150:200]) # 英
    char_lst.append('英')

    trimmed_imgs.append(img[:, 225:281]) # 樹
    char_lst.append('樹')


    return (trimmed_imgs, char_lst)


def main(filename):
    img = cv2.imread(os.path.join(test_dir, filename))
    print os.path.join(test_dir, filename)
    print img.shape
    trimmed_imgs, char_lst = trimming_test(img)

    resized_imgs = []
    for img in trimmed_imgs:
        resized_imgs.append(cv2.resize(img, (28, 28)))

    for i, img in enumerate(resized_imgs):
        cv2.imshow(str(i), img)
        cv2.imwrite(os.path.join(data_dir, char_lst[i] + '.jpg'), img)

    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main('0cards_10.jpg')
    #main('0cards_11.jpg')
