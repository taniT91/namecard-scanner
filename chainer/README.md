# Chainerを使った一文字認識

一文字に切り出された画像から文字を認識する．

## Requirement
python
- 2.7.12 :: Anaconda 2.4.0 (64-bit)

chainer
- 1.12.0

cuda
- 7.5  

## クローンしたらやること
**訓練データ用のディレクトリの作成**  
`mkdir characters_img`

**フォントディレクトリの作成**  
`mkdir fonts`

**フォントのダウンロード**  
先程作成した`./fonts`に使いたいフォントを入れる．  

**フォントから訓練データの作成**  
`python make_char_image.py`

## 実行
**学習**

`python learn_model.py`

GPUを使う場合は  
`python learn_model.py -g`

また，`-b`，`-e`オプションで，それぞれバッチサイズ，エポック数（学習回数）を指定可能


**テスト**

`python test_model.py`

GPUを使う場合は  
`python test_model.py -g`


## ディレクトリ構造とファイルの説明
重要なソースファイルは
- make_char_image.py
- learn_model.py
- test_model.py

````
chainer/
    |- characters_img/       # 訓練データ
    |   |- alpha_big/        # -------------------------------------------
    |   |- alpha_small/      #
    |   |- hiragana/         # make_char_image.pyで自動生成されるディレクトリ達
    |   |- katakana/         #
    |   |- num/              #
    |   |- kanji/            # -------------------------------------------
    |
    |- fonts/                # 訓練データ生成用のフォント置き場
    |
    |- imgs/                 # 色んな画像データを置いとく場所
    |   |                    # とりあえず置いとく用
    |   |
    |   |- test_data/        # テストデータ置き場
    |
    |- models/               # 学習されたモデル
    |
    |- jyojo_ichiran.txt     # 常用漢字2000字が入ったテキストファイル
    |                        # make_char_image.pyで使用
    |
    |- learn_model.py        # characters_imgの画像を学習，
    |                        # 求めたパラメータを models/ に保存
    |
    |- make_char_image.py    # fonts/ から学習データを生成
    |
    |- test_model.py         # models/ のモデルから ./imgs/test_data/の画像をテスト
    |
    |- trimming.py           # 手動でbounding boxから一文字切り出すためのプログラム
````

## モデルの作成
今回は既に`./models/my.model`に作成済みモデルを置いてある．  
このモデルを作成するために用いた環境は以下の通りである．

**実行環境**

AWS, g2-2xlarge  
この環境は私のバイト先で使っているものを貸して頂いた．
GPUの環境構築は自身で行った．

**学習データ**

今回は容量の関係上`./fonts/`にフォントを置いていない．  
上記の実行環境では331種類のフォントを用いて，英字大文字小文字，数字，常用漢字2000字，ひらがな，カタカナをそれぞれ作成し，計782,815個の訓練データを作成した．
