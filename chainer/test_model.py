# -*- coding: utf-8 -*

import numpy as np
from chainer import cuda, Variable, FunctionSet, optimizers, serializers
import chainer.functions  as F
import sys
import os
import cv2
from itertools import chain
import codecs
import argparse

np.set_printoptions(threshold=np.inf)
test_dir = './imgs/'

alpha_small_lst = [chr(i) for i in range(97, 97+26)]
alpha_big_lst = [chr(i) for i in range(65, 65+26)]
num_lst = [chr(i) for i in range(48, 48+10)]
hiragana_lst = [unichr(i) for i in range(12353, 12436)]
katakana_lst = [unichr(i) for i in range(12449, 12532+1)]
kanji_lst = []
for kanji in codecs.open('jyoyo_ichiran.txt', 'r', 'utf-8'):
    kanji_lst.append(kanji.strip())
char_lst = list(chain.from_iterable([alpha_small_lst, alpha_big_lst, num_lst, hiragana_lst, katakana_lst, kanji_lst]))


def load_test_data(file_dir):
    x_train = []
    y_train = []
    target_dir = test_dir + file_dir

    for file in os.listdir(target_dir):
        if file == '.DS_Store': continue
        src = cv2.imread(os.path.join(target_dir, file), 0)
        ret, x = cv2.threshold(src, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)

        x_train.append(x)
        index = char_lst.index(unicode(file, 'utf-8')[0])
        y_train.append(index)

    return (x_train, y_train)


# Prepare multi-layer perceptron model
# 多層パーセプトロンモデルの設定
# 入力 784次元、出力 y_num次元
def init_model(n_units, y_num):
    return FunctionSet(l1=F.Linear(784, n_units),
                       l2=F.Linear(n_units, n_units),
                       l3=F.Linear(n_units, y_num))


# Convolutional Neural Network(CNN)
def init_cnn_model(y_num):
    return FunctionSet(conv1=F.Convolution2D(1, 20, 5),
                       conv2=F.Convolution2D(20, 50, 5),
                       l1=F.Linear(800, 500),
                       l2=F.Linear(500, y_num))


def forward(x_data, y_data, train=True):
    x, t = Variable(x_data), Variable(y_data)
    h1 = F.dropout(F.relu(model.l1(x)),  train=train)
    h2 = F.dropout(F.relu(model.l2(h1)), train=train)
    y  = model.l3(h2)
    return F.softmax_cross_entropy(y, t), F.accuracy(y, t)


# CNNのforward
def forward_cnn(model, x_data, y_data, train=True):
    x, t = Variable(x_data), Variable(y_data)
    h = F.max_pooling_2d(F.relu(model.conv1(x)), 2)
    h = F.max_pooling_2d(F.relu(model.conv2(h)), 2)
    h = F.dropout(F.relu(model.l1(h)), train=train)
    y = model.l2(h)
    return F.softmax_cross_entropy(y, t), F.accuracy(y, t), y


def forward_one(xp, x_data):
    xxx = xp.asarray(x_data.astype(xp.float32))
    h1 = F.dropout(F.relu(model.l1(Variable(xxx.reshape(1, 784)))),  train=False)
    h2 = F.dropout(F.relu(model.l2(h1)), train=False)
    y  = model.l3(h2)
    return y


def main():
    parser = argparse.ArgumentParser(description='Chainer')
    parser.add_argument('--gpu', '-g', type=int, default=-1,
                        help='GPU ID (negative value indicates CPU)')
    args = parser.parse_args()

    gpu_id = args.gpu

    if gpu_id >= 0:
        cuda.check_cuda_available()
    xp = cuda.cupy if gpu_id >= 0 else np

    x_test, y_test = load_test_data('test_data')
    x_test = np.array(x_test).astype(xp.float32).reshape((len(x_test), 1, 28, 28)) / 255
    y_test = np.array(y_test).astype(xp.int32)
    N_test = len(x_test)

    # 中間層の数
    n_units   = 5000
    # 出力層の数
    y_num = len(char_lst)
    model = init_cnn_model(y_num)

    if gpu_id >= 0:
        cuda.get_device(gpu_id).use()
        model.to_gpu()
    print "loading model..."
    serializers.load_npz('./models/my.model', model)

    # evaluation
    loss, acc, y = forward_cnn(model, xp.asarray(x_test), xp.asarray(y_test), train=False)

    print("correct | answer")
    for i, y in enumerate(cuda.to_cpu(y.data)):
        y = np.argsort(y)[-1::-1][:3]
        results = [char_lst[e].encode('utf-8') for e in y]

        #miss = "" if y_test[i] == np.argmax(y) else "x"
        miss = " " if y_test[i] == y[0] else "x"

        print "   {}    |   {} {} {} {}".format(char_lst[y_test[i]].encode('utf-8'),
                                                miss,
                                                results[0],
                                                results[1],
                                                results[2])


if __name__ == '__main__':
    main()
