# -*- coding: utf-8 -*-

import sys
import os
import codecs
import re
import numpy as np
from PIL import ImageFont
from PIL import Image
from PIL import ImageDraw

output_dir = "./characters_img/"


def write(dir, filename, img):
    target_dir = os.path.join(output_dir, dir)
    if not os.path.isdir(target_dir):
        os.makedirs(target_dir)
    img.save(os.path.join(target_dir, filename))


def write_list_img(dir, lst, font, fontname):
    for e in lst:
        filename = e + '_' + fontname +'.jpg'
        w, h = font.getsize(e)
        im = Image.new('RGB', (28, 28), (255,255,255))
        draw = ImageDraw.Draw(im)
        img_size = np.array(im.size)
        txt_size = np.array(font.getsize(e))
        pos = (img_size - txt_size)/2
        draw.text(pos, e, fill=(0,0,0), font=font)
        write(dir, filename, im)


# 英語小文字のリスト
alpha_small_lst = [chr(i) for i in range(97, 97+26)]
# 英語大文字のリスト
alpha_big_lst = [chr(i) for i in range(65, 65+26)]
# 数字のリスト
num_lst = [chr(i) for i in range(48, 48+10)]
# ひらがなのリスト
hiragana_lst = [unichr(i) for i in range(12353, 12436)]
# カタカナのリスト
katakana_lst = [unichr(i) for i in range(12449, 12532+1)]
# 漢字のリスト
kanji_lst = []
for kanji in codecs.open('jyoyo_ichiran.txt', 'r', 'utf-8'):
    kanji_lst.append(kanji.strip())

pattern = '\..*'
not_font_lst = ['encodings.dir', 'fonts.dir', 'fonts.list', 'fonts.scale']
for fontfile in os.listdir('./fonts'):
    if fontfile != '.DS_Store' and fontfile not in not_font_lst:
        fontfile = fontfile.decode('utf-8')
        print fontfile
        fontpath = os.path.join('./fonts', fontfile)
        font = ImageFont.truetype(fontpath, 28, encoding='utf-8')
        fontfile = re.sub(pattern, '', fontfile)
        print 'alpha_small'
        write_list_img('alpha_small', alpha_small_lst, font, fontfile.replace(' ', ''))
        print 'alpha_big'
        write_list_img('alpha_big', alpha_big_lst, font, fontfile.replace(' ', ''))
        print 'num'
        write_list_img('num', num_lst, font, fontfile.replace(' ', ''))
        print 'hiragana'
        write_list_img('hiragana', hiragana_lst, font, fontfile.replace(' ', ''))
        print 'katakana'
        write_list_img('katakana', katakana_lst, font, fontfile.replace(' ', ''))
        print 'kanji'
        write_list_img('kanji', kanji_lst, font, fontfile.replace(' ', ''))
