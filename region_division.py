# -*- coding: utf-8 -*

import numpy as np
import cv2
import pylab as plt
import copy
import random
import sys
import os
import os.path
import connected_component
import union_find
import geometory

img_dir = "./processed/out"
output_dir = "./region_division"

def write(dir, file, img):
    target_dir = os.path.join(output_dir, dir)
    if not os.path.isdir(target_dir):
        os.makedirs(target_dir)
    print os.path.join(target_dir, file)
    cv2.imwrite(os.path.join(target_dir, file), img)


def write_graph(dir, file, img):
    target_dir = os.path.join(output_dir, dir)
    if not os.path.isdir(target_dir):
        os.makedirs(target_dir)
    print os.path.join(target_dir, file)
    plt.savefig(os.path.join(target_dir, file))


def calc_threshold(hist):
    hist = list(hist)
    data = []
    for e in hist:
        data.extend(e)

    thre = 1200
    max_index = data.index(max(data))
    #print "max_index ", max_index
    if max_index < 122:
        for i in range(max_index, len(data)):
            if data[i] < thre: return ['r', i]
    else:
        data.reverse()
        max_index = len(data) - max_index
        #print "max_index ", max_index
        for i in range(max_index, len(data)):
            if data[i] < thre: return ['l', len(data) - i]


def plot_box(img, cc):

    for c in cc:
        pass


def devide(f, src):
    # img = cv2.imread('./processed/out/0cards3.jpg', cv2.IMREAD_GRAYSCALE)
    # height = len(img)
    # width = len(img[0])
    # thresh = 200
    # max_pixel = 255
    # ret, img = cv2.threshold(img,
    #                          thresh,
    #                          max_pixel,
    #                          cv2.THRESH_BINARY)

    # kernel = np.ones((5, 5), np.float32)/25
    # img = cv2.filter2D(img, -1, kernel)


    # MSERによる領域分割
    # vis = img.copy()
    # mser = cv2.MSER_create()
    # regions = mser.detectRegions(img, None)
    # hulls = [cv2.convexHull(p.reshape(-1, 1, 2)) for p in regions]
    # cv2.polylines(vis, hulls, 1, (0, 255, 0))
    # cv2.imshow('img', vis)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    # エッジ検出
    # canny_edges = cv2.Canny(img,100,200)
    #
    # #結果表示
    # cv2.imshow('canny_edges',canny_edges)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    if src is None:
        print("Failed to load image file.")
        sys.exit(1)

    height, width, channels = src.shape[:3]
    dst = copy.copy(src)
    gray = cv2.cvtColor(src, cv2.COLOR_BGR2GRAY)
    cut_range = 10
    gray = gray[cut_range:height - cut_range, cut_range:width - cut_range]

    # 回転
    # rows, cols = gray.shape
    # M = cv2.getRotationMatrix2D((cols/2,rows/2), 90, 1)
    # gray = cv2.warpAffine(gray, M, (cols, rows))
    # gray = gray.T

    write("gray", f, gray)

    # ヒストグラム
    #hist = cv2.calcHist([gray], [0], None, [256], [0, 256])
    #threshold_lst = calc_threshold(hist)
    #print(threshold_lst[1])
    # plt.figure()
    # plt.plot(hist)
    # plt.xlim([0,256])
    # plt.show()
    # write_graph('histgram', f, hist)

    # ret, bin = cv2.threshold(gray, threshold_lst[1], 255, cv2.THRESH_BINARY)
    # if threshold_lst[0] == 'l':
    #     bin = 255 - bin

    bin = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 5, 5)
    bin = 255 - bin

    # kernel = np.ones((2, 2), np.uint8)
    # img = cv2.filter2D(img, -1, kernel)
    # bin = cv2.morphologyEx(bin, cv2.MORPH_OPEN, kernel)

    write("binary", f, bin)

    # bin = cv2.erode(bin, kernel, iterations = 1)
    # bin = cv2.dilate(bin, kernel, iterations = 1)
    #
    # write("unnoised", f, bin)
    # kernel = np.ones((10, 10), np.float32)/100
    # img = cv2.filter2D(bin, -1, kernel)

    # cv2.imshow('gray', gray)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    cc = connected_component.detect(bin, src)
    colored = cc.colored()
    #write("colored", f, colored)

    uf = union_find.UnionFind(len(cc.components))
    for i, c in enumerate(cc.components):
        for j in xrange(i+1, len(cc.components)):
            d = cc.components[j]
            c_xmin, c_xmax, c_ymin, c_ymax = c.left, c.left + c.width, c.top, c.top + c.height
            d_xmin, d_xmax, d_ymin, d_ymax = d.left, d.left + d.width, d.top, d.top + d.height
            if geometory.included_orth(c_xmin, c_xmax, d_xmin, d_xmax) or c.height >= 500 or d.height >= 500:
                continue
            if geometory.distance_orth(c_ymin, c_ymax, d_ymin, d_ymax) == 0 and geometory.distance_orth(c_xmin, c_xmax, d_xmin, d_xmax) <= 100:
                uf.union(i, j)

    connected = cc.colored(uf)
    #write("connected", f, connected)
    #src = cc.bounding_box_on_original(src, uf, cut_range, 1)
    #write("bounding_box", f, src)

    boxes = cc.calc_bounding_box(uf)
    box_imges = []
    margin = 1
    for i, box in enumerate(boxes):
        bounding_box_img = src[box['top'] - margin + cut_range:box['bottom'] + margin + cut_range,
                               box['left'] - margin + cut_range:box['right'] + margin + cut_range]
        #bounding_box_img = bin[box['top'] - margin:box['bottom'] + margin, box['left'] - margin:box['right'] + margin]
        end_name = "_" + str(i) + ".jpg"
        f_new = f.replace(".jpg", end_name)
        write("bounding_box_img", f_new, bounding_box_img)
        box_imges.append(bounding_box_img)


if __name__ == '__main__':
    filenames = []
    for file in os.listdir(img_dir):
        if os.path.isfile(os.path.join(img_dir, file)) and file != ".DS_Store" and file.endswith("cards.jpg"):
            filenames.append(file)
    for filename in filenames:
        src = cv2.imread(os.path.join(img_dir, filename), cv2.IMREAD_COLOR)
        devide(filename, src)
