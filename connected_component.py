# -*- coding: utf-8 -*
import cv2
import copy
import circular_list as circular
import math
import numpy as np
import random
import geometory

class ComponentGroup:
    def __init__(self, img, original = None, min_connected_area = 0):
        self.img = img
        self.min_connected_area = min_connected_area
        self.height, self.width = img.shape[:2]
        self.processed = original if original is not None else np.zeros((self.height, self.width, 3), np.uint8) # copy.copy(self.img)
        self.label_size, self.labeled, self.stats, self.centroids = cv2.connectedComponentsWithStats(img, connectivity=4)
        self.lines = cv2.HoughLinesP(img, rho=5, theta=math.pi / 180.0, threshold=100, minLineLength=50, maxLineGap=10)
        self.points = self._points()
        self.components = self._components()

    def colored(self, uf = None):
        colors = create_colors(self.label_size) # geometory.color_array(self.label_size)
        colored = np.zeros((self.height, self.width, 3), np.uint8)
        for y in range(self.height):
            for x in range(self.width):
                v = self.labeled[y, x]
                if v > 0:
                    if self.stats[v, cv2.CC_STAT_AREA] >= self.min_connected_area:
                        c = v if uf is None else uf.find(v-1)
                        colored[y, x] = colors[c]
                    else:
                        colored[y, x] = [255, 255, 255]
        return colored

    def lined(self, img):
        res = copy.copy(img)
        if self.lines is not None:
            for line in self.lines:
                for (x1, y1, x2, y2) in line:
                    cv2.line(res, (x1, y1), (x2, y2), (0, 255, 0), 1)
        return res

    def _points(self):
        points = [[] for _ in range(self.label_size)]
        for y in range(self.height):
            for x in range(self.width):
                v = self.labeled[y, x]
                if v > 0:
                    points[v].append((x,y))
        return [np.array(ps) for ps in points]

    def _components(self):
        comps = []
        for l in range(1, self.label_size):
            if self.stats[l, cv2.CC_STAT_AREA] >= self.min_connected_area:
                comps.append(Component(self.processed, l, self.labeled, self.stats[l], self.centroids[l], self.points[l]))
        return comps

    def calc_bounding_box(self, uf):

        def search_dic_index(dic_list, id):
            for i, dic in enumerate(dic_list):
                if dic['id'] == id:
                    return i

        componet_num = len(filter(lambda e: e < 0, uf.table))
        boxes = [{'id':-1, 'left': -1, 'top': -1, 'right': -1, 'bottom': -1} for i in range(componet_num)]
        index = 0
        for i, parent_id in enumerate(uf.table):
            c = self.components[i]
            if parent_id < 0:
                boxes[index]['id'] = i
                boxes[index]['left'] = c.left
                boxes[index]['top'] = c.top
                boxes[index]['right'] = c.left + c.width
                boxes[index]['bottom'] = c.top + c.height
                index += 1
                continue
            box_index = search_dic_index(boxes, parent_id)
            if boxes[box_index]['left'] > c.left:
                boxes[box_index]['left'] = c.left
            if boxes[box_index]['top'] > c.top:
                boxes[box_index]['top'] = c.top
            if boxes[box_index]['right'] < c.left + c.width:
                boxes[box_index]['right'] = c.left + c.width
            if boxes[box_index]['bottom'] < c.top + c.height:
                boxes[box_index]['bottom'] = c.top + c.height
        return boxes

    def bounding_box_on_original(self, img, uf, cut_range=0, margin=0):
        boxes = self.calc_bounding_box(uf)
        for box in boxes:
            cv2.rectangle(img, (box['left'] + cut_range - margin, box['top'] + cut_range - margin),
                            (box['right'] + cut_range + margin, box['bottom'] + cut_range + margin), (255,255,0), 2)
        return img


class Component:
    def __init__(self, img, id, labeled, stat, centroid, points):
        self.img = img
        self.img_height, self.img_width = img.shape[:2]
        self.id = id
        self.labeled = labeled
        self.left = stat[cv2.CC_STAT_LEFT]
        self.top = stat[cv2.CC_STAT_TOP]
        self.width = stat[cv2.CC_STAT_WIDTH]
        self.height = stat[cv2.CC_STAT_HEIGHT]
        self.area = stat[cv2.CC_STAT_AREA]
        self.centroid = centroid
        self.points = points

    def __repr__(self):
        return "component[%d]" % self.id

    def detect_4points(self):
        hull = cv2.convexHull(self.points)
        size = len(hull)
        max_area = 0
        max_pts = []
        for i in xrange(len(hull)):
            for j in xrange(i+1, len(hull)):
                for k in xrange(j+1, len(hull)):
                    for l in xrange(k+1, len(hull)):
                        pts = np.array([hull[i], hull[j], hull[k], hull[l]])
                        area = cv2.contourArea(pts)
                        if max_area < area:
                            max_area = area
                            max_pts = pts

        cv2.polylines(self.img, [max_pts], 1, (255,0,255), 2)
        for p in hull:
            cv2.circle(self.img, tuple(p[0]), 2, (0,255,0), -1)
        return max_pts

    def detect_by_area(self, n = 4):
        hull = cv2.convexHull(self.points)
        removed = [False] * len(hull)
        size = len(hull)
        while size > n:
            min_area = 1234567890
            min_i = -1
            for i, p in enumerate(hull):
                area = cv2.contourArea(np.array([hull[i-1], p, hull[(i+1)%size]]))
                if min_area > area:
                    min_area = area
                    min_i = i
            hull = [p for i, p in enumerate(hull) if i != min_i]
            size -= 1
        print len(hull), min_area
        cv2.polylines(self.img, [np.array(hull)], 1, (255,0,255), 2)

    def detect_rect(self):
        rotrect = cv2.minAreaRect(self.points)
        box = cv2.boxPoints(rotrect)
        box = np.int0(box)
        #cv2.circle(self.img, (int(round(rotrect[0][0])), int(round(rotrect[0][1]))), 5, (255,255,255), -1)
        cv2.polylines(self.img, [box], 1, (255,0,255), 2)

    def detect_corners(self):
        class HullPoint:
            def __init__(self, point, theta, prev_distance, next_distance):
                self.point = point
                self.theta = theta
                self.prev_distance = prev_distance
                self.next_distance = next_distance
            def __repr__(self):
                return "%s[%d]<%d>" % (tuple(self.point), int(round(self.theta)), self.next_distance)
        hull = cv2.convexHull(self.points)
        size = len(hull)
        p = hull[size-2][0]
        q = hull[size-1][0]
        max_theta = -1
        max_theta_i = -1
        hull_points = []
        for i, rr in enumerate(hull):
            r = rr[0]
            dot = np.dot(q-p, r-q)
            pq = np.dot(q-p, q-p)
            rq = np.dot(r-q, r-q)
            cos = dot / math.sqrt(pq * rq)
            theta = np.arccos(cos) / math.pi * 180
            if theta > max_theta:
                max_theta = theta
                max_theta_i = i
            hull_points.append(HullPoint(q, theta, pq, rq))
            p = q
            q = r
        cl = circular.List(hull_points)

        ### merge corners ###
        def merge_next(x, y):
            point = x.point if x.theta > y.theta else y.point
            return HullPoint(point, x.theta + y.theta, x.prev_distance, y.prev_distance)
        def merge_prev(x, y):
            return merge_next(y, x)
        seen = None
        node = cl.node
        while node is not seen:
            if node.content.theta > 20:
                while node.content.next_distance <= 25:  # less than 5px
                    node = node.absorb(1, merge_next)
                    seen = node
            node = node.next

        hull_ps = cl.to_python_list()
        points = map(lambda x: x.point, hull_ps)
        print len(hull_points), hull_points
        print len(points), hull_ps
        print '---'
        cv2.polylines(self.img, geometory.to_pts(points), 1, (255,0,255), 2)


    def detect_cos(self):
        hull = cv2.convexHull(self.points)
        size = len(hull)
        p = hull[size-2][0]
        q = hull[size-1][0]
        thetas = []
        for i, rr in enumerate(hull):
            r = rr[0]
            dot = np.dot(q-p, r-q)
            cos = dot / math.sqrt(np.dot(q-p, q-p) * np.dot(r-q, r-q))
            thetas.append(np.arccos(cos) / math.pi * 180)
            p = q
            q = r
        # print thetas
        thresh = 1
        while thresh <= 80:
            cnt = 0
            prev_low = thetas[-1] < thresh
            for i, theta in enumerate(thetas):
                low = theta < thresh
                if prev_low and not low:
                    cnt += 1
                prev_low = low
            if cnt == 4:
                break
            thresh += 5

        lines = []
        point_group = []
        prev_low = thetas[-1] < thresh
        if prev_low:
            for i in range(1, len(thetas)):
                if thetas[-i] < thresh:
                    point_group.append(hull[size-i-1][0])
                    low_start = -i
                else:
                    break
        for i, theta in enumerate(thetas):
            low = theta < thresh
            if low:
                point_group.append(hull[i-1][0])
            elif prev_low:
                point_group.append(hull[i-1][0])
                lines.append(geometory.regression_line(point_group))
                point_group = []
            prev_low = low

        if len(lines):
            # print thresh, thetas
            corners = geometory.corners(lines)
            cv2.polylines(self.img, geometory.to_pts(corners), 1, (255,0,255), 2)
            for l in lines:
                cv2.line(self.img, tuple(l[0]), tuple(l[1]), (0,255,0), 2)


    def detect_polylines(self):
        hull = cv2.convexHull(self.points)
        size = len(hull)
        last = size - 1
        p = hull[last][0]
        q = hull[0][0]
        v = q - p
        right, top, left = last, last, last
        rightv, topv, leftv = 0, 0, 0
        for i, zz in enumerate(hull):
            z = zz[0]
            dot = np.dot(v, z-p)
            det = np.cross(v, z-p)
            if dot > rightv:
                rightv = dot
                right = i
            if dot < leftv:
                leftv = dot
                left = i
            if det > topv:
                topv = det
                top = i
        min_area = topv * (rightv - leftv) / np.dot(v, v)**2
        min_res = np.array([np.array([p]), np.array([q]), hull[right], hull[top], hull[left]])

        for i, qq in enumerate(hull):
            if i == 0:
                p = q
                continue
            q = qq[0]
            v = q - p
            rightv = np.dot(v, hull[right][0]-p)
            leftv = np.dot(v, hull[left][0]-p)
            topv = np.cross(v, hull[top][0]-p)
            for k in range(1, size):
                rightvv = np.dot(v, hull[(right+k)%size][0]-p)
                if rightvv > rightv:
                    rightv = rightvv
                    right = (right+k)%size
                else:
                    break
            for k in range(1, size):
                leftvv = np.dot(v, hull[(left+k)%size][0]-p)
                if leftvv < leftv:
                    leftv = leftvv
                    left = (left+k)%size
                else:
                    break
            for k in range(1, size):
                topvv = np.cross(v, hull[(top+k)%size][0]-p)
                if topvv > topv:
                    topv = topvv
                    top = (top+k)%size
                else:
                    break
            area = topv * (rightv - leftv) / np.dot(v, v)**2
            if area < min_area:
                min_area = area
                min_res = np.array([np.array([p]), np.array([q]), hull[right], hull[top], hull[left]])
            p = q
        cv2.polylines(self.img, [min_res], 1, (255,0,255), 2)
        #cv2.polylines(self.img, [hull], 1, (255,0,255), 2)


    def detect_sides(self):
        x, y = self._find_top_pixel()
        step = 1
        deg_size =  360 / step
        check_cnt = [1] * deg_size
        rest = deg_size
        for r_i in range(100):
            if rest <= 2:
                break
            for deg_i in range(deg_size):
                if check_cnt[deg_i] < 0:
                    continue;
                theta = math.pi/180 * deg_i * step
                r = (r_i + 1) * 2.0
                tx = round(x + r * math.cos(theta))
                ty = round(y + r * math.sin(theta))
                if tx < 0 or ty < 0 or tx >= self.img_width or ty >=self.img_height:
                    rest -= 1
                    continue
                if self.labeled[ty, tx] == self.id:
                    check_cnt[deg_i] += 1
                if r_i - check_cnt[deg_i] > max(3, deg_i*0.1):
                    check_cnt[deg_i] *= -1
                    rest -= 1

        for deg_i in range(deg_size):
            if check_cnt[deg_i] < 0:
                continue;
            theta = math.pi/180 * deg_i * step
            cv2.line(self.img, (x, y), (int(round(x+check_cnt[deg_i]*2*math.cos(theta))), int(round(y+check_cnt[deg_i]*2*math.sin(theta)))),(255,0,0),5)

    def _find_top_pixel(self):
        for x in range(self.left, self.left + self.width):
            if self.labeled[self.top, x]:
                return (x, self.top)


class ConnectedComponent:
    def __init__(self, img, id, labeled, stat, centroid, points):
            self.img = img
            self.img_height, self.img_width = img.shape[:2]
            self.id = id
            self.labeled = labeled
            self.left = stat[cv2.CC_STAT_LEFT]
            self.top = stat[cv2.CC_STAT_TOP]
            self.width = stat[cv2.CC_STAT_WIDTH]
            self.height = stat[cv2.CC_STAT_HEIGHT]
            self.area = stat[cv2.CC_STAT_AREA]
            self.centroid = centroid
            self.points = points


def create_colors(n):
    return [np.array([random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)]) for i in range(n)]

def detect(img, original = None, min_connected_area = 0):
    return ComponentGroup(img, original, min_connected_area)
